//
// Version: 0.16.0
//
var loadData = function () {
    document.getElementById('subject').innerHTML = keycloak.subject;

    if (keycloak.tokenParsed) {
        document.getElementById('profileType').innerHTML = 'IDToken';
        document.getElementById('username').innerHTML = keycloak.tokenParsed.preferred_username;
        document.getElementById('email').innerHTML = keycloak.tokenParsed.email;
        document.getElementById('name').innerHTML = keycloak.tokenParsed.name;
        document.getElementById('givenName').innerHTML = keycloak.tokenParsed.given_name;
        document.getElementById('familyName').innerHTML = keycloak.tokenParsed.family_name;
        document.getElementById('status').innerHTML = '<b>Logged</b>';
    }
};

var loadFailure = function () {
    document.getElementById('status').innerHTML = '<b>Failed to load data.  Check console log</b>';
};

var reloadData = function () {
    console.log('ReloadData');
    keycloak.updateToken(10)
    .success(loadData)
    .error(function() {
        document.getElementById('status').innerHTML = '<b>Error updating Token. Failed to load data.  User is logged out.</b>';
    });
};
//-------------------------------------------------------------------------------------------------------------

var testGetMethods = function (){
    var selectedOption = $('#get_selection option:selected').val();
    switch (selectedOption) {
        case 'cap':
            testGetCapability();
            break;
        case 'captyp':
            testGetCapabilityTypes();
            break;
        case 'bp':
            testGetBusinessProcesses();
            break;
        case 'bpt':
            testGetBusinessProcessTemplate();
            break;
    }
};

//using fispace library output as text
var testGetCapabilityTypes = function () {
    var target =document.getElementById('targetNumber').value;
    JsConnector.getCapabilityTypes(target, function(text){
        $('#chart_CT').empty();
        if(target !== "") {
            $('#chart_CT').append(text);
        }
        else {
            $('#chart_CT').append('<tr><th>Id</th><th>Name</th><th>identifier</th><th>requestMessageType</th><th>responseMessageType</th><th>schemaLocation</th><th>contextPath</th></tr>');
            $(text).find('ns2\\:capabilityType').each(function(){
                var Col0 = $(this).find('ns2\\:id').text();
                var Col1 = $(this).find('ns2\\:name').text();
                var Col11 = $(this).find('ns2\\:identifier').text();
                var Col2 = $(this).find('ns2\\:requestMessageType').text();
                var Col3 = $(this).find('ns2\\:responseMessageType').text();
                var Col4 = $(this).find('ns2\\:schemaLocation').text();
                var Col5 = $(this).find('ns2\\:contextPath').text();
                $('<tr></tr>').html('<th>'+Col0+'</th><td>'+Col1+'</td><td>'+Col11+'</td><td>'+Col2+'</td><td>'+Col3+'</td><td>'+Col4+'</td><td>'+Col5+'</td>').appendTo('#chart_CT');
            });
        }
    });
};

var testGetCapability = function () {
    JsConnector.getCapabilities(document.getElementById('targetNumber').value, function(text){
        $('#chart_CT').empty();
        $('#chart_CT').append('<tr><th>Id</th><th>Description</th><th>Uri</th><th>cte_id</th></tr>');
        var Col0 = $(text).find('ns2\\:id').text();
        var Col1 = $(text).find('ns2\\:description').text();
        var Col2 = $(text).find('ns2\\:uri').text();
        var Col3 = $(text).find('ns2\\:cte_id').text();
        $('<tr></tr>').html('<th>'+Col0+'</th><td>'+Col1+'</td><td>'+Col2+'</td><td>'+Col3+'</td>').appendTo('#chart_CT');
   });
};

var testGetBusinessProcesses = function () {
    var target =document.getElementById('targetNumber').value;
    JsConnector.getBusinessProcesses(target, function(text){
        $('#chart_CT').empty();
        if(target !== "") {
            $('#chart_CT').append(text);
        }
        else {
            $('#chart_CT').append('<tr><th>Id</th><th>Name</th><th>bpt_id</th><th>Capabilities</th></tr>');
            $(text).find('ns2\\:businessProcess').each(function(){
                var Col0 = $(this).find('ns2\\:id').first().text();
                var Col1 = $(this).find('ns2\\:name').text();
                var Col2 = $(this).find('ns2\\:bpt_id').text();
                var Col3 = 0;
                $(this).find('ns2\\:capabilities').each(function(){
                    Col3 = Col3 + 1;
                });
                $('<tr></tr>').html('<th>'+Col0+'</th><td>'+Col1+'</td><td>'+Col2+'</td><td>'+Col3+'</td>').appendTo('#chart_CT');
            });
        }
    });
};

var testGetBusinessProcessTemplate = function () {
    var target =document.getElementById('targetNumber').value;
    JsConnector.getBusinessProcessTemplates(target, function(text){
        $('#chart_CT').empty();
        if(target !== "") {
            $('#chart_CT').append(text);
        }
        else {
            $('#chart_CT').append('<tr><th>Id</th><th>Name</th></tr>');
            $(text).find('ns2\\:businessProcessTemplate').each(function(){
                var Col0 = $(this).find('ns2\\:id').text();
                var Col1 = $(this).find('ns2\\:name').text();
                $('<tr></tr>').html('<th>'+Col0+'</th><td>'+Col1+'</td>').appendTo('#chart_CT');
            });
        }
    });
};

var testPutCapabilityType = function (formData) {
    JsConnector.putNewCapabilityType(formData,function(text){
       document.getElementById('response').innerHTML = "<p>"+text+"</p>";
   });
    return false;
};


var testPutCapability = function (formData) {
    JsConnector.putNewCapability(formData,function(text){
       document.getElementById('response').innerHTML = "<p>"+text+"</p>";
   });
    return false;
};

var testPutBusinessProcessTemplate = function (formData) {
    JsConnector.putNewBusinessProcessTemplate(formData,function(text){
       document.getElementById('response').innerHTML = "<p>"+text+"</p>";
   });
    return false;
};

var testPutBusinessProcessTemplateWithRoles = function (formData) {
    var bptRoles = [];
    //Roles array creation, role object contains name and cap_types_ids fields
    bptRoles.push({name: formData.role1_name.value,
        cap_type_ids: formData.role1_cap_type_ids.value.split(" ")});
    bptRoles.push({name: formData.role2_name.value,
        cap_type_ids: formData.role2_cap_type_ids.value.split(" ")});
    JsConnector.putNewBusinessProcessTemplateWithRoles(formData,bptRoles,function(text){
       document.getElementById('response').innerHTML = "<p>"+text+"</p>";
   });
    return false;
};

var testPutBusinessProcess = function (formData) {
    JsConnector.putNewBusinessProcess(formData,function(text){
       document.getElementById('response').innerHTML = "<p>"+text+"</p>";
   });
    return false;
};

var testPutBusinessProcessWithRoles = function (formData) {
    var bpRoles = [];
    //Roles array creation, role object contains name and cap_ids fields
    bpRoles.push({name: formData.role1_name.value,
        cap_ids: formData.role1_cap_ids.value.split(" ")});
    bpRoles.push({name: formData.role2_name.value,
        cap_ids: formData.role2_cap_ids.value.split(" ")});
    JsConnector.putNewBusinessProcessWithRoles(formData,bpRoles,function(text){
       document.getElementById('response').innerHTML = "<p>"+text+"</p>";
   });
    return false;
};

var testDeleteMethods = function (){
    var selectedOption = $('#get_selection_delete option:selected').val();
    switch (selectedOption) {
        case 'cap':
            testDeleteCapability();
            break;
        case 'captyp':
            testDeleteCapabilityType();
            break;
        case 'bp':
            testDeleteBusinessProcess();
            break;
        case 'bpt':
            testDeleteBusinessProcessTemplate();
            break;
    }
};

var testDeleteCapability = function () {
    JsConnector.deleteCapability(document.getElementById('targetDeleteNumber').value,function(text){
       document.getElementById('response2').innerHTML = "<p>"+text+"</p>";
   });
};

var testDeleteCapabilityType = function () {
    JsConnector.deleteCapabilityType(document.getElementById('targetDeleteNumber').value,function(text){
       document.getElementById('response2').innerHTML = "<p>"+text+"</p>";
   });
};

var testDeleteBusinessProcess = function () {
    JsConnector.deleteBusinessProcess(document.getElementById('targetDeleteNumber').value,function(text){
       document.getElementById('response2').innerHTML = "<p>"+text+"</p>";
   });
};

var testDeleteBusinessProcessTemplate = function () {
    JsConnector.deleteBusinessProcessTemplate(document.getElementById('targetDeleteNumber').value,function(text){
       document.getElementById('response2').innerHTML = "<p>"+text+"</p>";
   });
};

//Test use capability receive_shipment_status
var testPostUse = function (formData) {
    var postBody = formData.bodytext.value;
    JsConnector.useCapability(formData.captypename.value,postBody,function(text){
       document.getElementById('response2').innerHTML = "<p>"+text+"</p>";
   });
    return false;
};

$( "#get_selection" ).change(function() {
    $('#targetNumber').val("");
});