//
// Version: 0.16.0
//
(function(window){
    'use strict';

    //GET XML
    function getAsText(getUrl, callback){
        var req = new XMLHttpRequest();
        req.open('GET', getUrl, true);
        //req.withCredentials = true;
        req.setRequestHeader('Accept', 'application/xml');
        req.setRequestHeader('Authorization', 'Bearer ' + keycloak.token);
        req.onreadystatechange = function () {
            if (req.readyState == 4) {
                console.log('finished loading data: '+req.responseText);
                    callback(req.responseText);
            }
        };
        req.send();
    }
    //GET JSON
    function getAsJsonText(getUrl, callback){
        var req = new XMLHttpRequest();
        req.open('GET', getUrl, true);
        //req.withCredentials = true;
        req.setRequestHeader('Accept', 'application/json');
        req.setRequestHeader('Authorization', 'Bearer ' + keycloak.token);
        req.onreadystatechange = function () {
            if (req.readyState == 4) {
                console.log('finished loading data: '+req.responseText);
                    callback(req.responseText);
            }
        };
        req.send();
    }
    //PUT
    function putContent(bodyContent, getUrl, callback){

        var req = new XMLHttpRequest();
        req.open('PUT', getUrl, true);
        //req.withCredentials = true;
        req.setRequestHeader("Content-type","application/xml");
        req.setRequestHeader('Accept', 'application/xml');
        req.setRequestHeader('Authorization', 'Bearer ' + keycloak.token);
        req.onload = function () {
            console.log('Post finished, response: '+req.responseText);
            callback(req.responseText);
        };
        console.log('Sending request body: '+bodyContent);
        req.send(bodyContent);
    }
    //POST
    function postContent(bodyContent, postUrl, callback){

        var req = new XMLHttpRequest();
        req.open('POST', postUrl, true);
        //req.withCredentials = true;
        req.setRequestHeader("Content-type","application/xml");
        req.setRequestHeader('Accept', 'application/xml');
        req.setRequestHeader('Authorization', 'Bearer ' + keycloak.token);
        req.onload = function () {
            console.log('Post finished, result: '+req.status+'. Response: '+req.responseText);
            callback('Result: '+req.status+'.  '+req.responseText);
        };
        req.onerror = function () {
            console.log('ERROR: CORS or keycloak problem.');
            callback('Error, check console.');
        };
        console.log('Sending request body: '+bodyContent);
        req.send(bodyContent);
    }
    //DELETE
    function sendDelete(deleteUrl, callback){
        var req = new XMLHttpRequest();
        req.open('DELETE', deleteUrl, true);
        //req.withCredentials = true;
        req.setRequestHeader('Authorization', 'Bearer ' + keycloak.token);
        req.onload = function () {
            console.log('Delete finished, response: '+req.responseText);
            callback('Response: ' + req.responseText);
        };
        console.log('Sending delete request to : '+deleteUrl);
        req.send();
    }
    //HELP FUNCTIONS
    function getCapType(getUrl){
        console.log('Getting capType for: '+getUrl);
        var req = new XMLHttpRequest();
        req.open('GET', getUrl, false);
        //req.withCredentials = true;
        req.setRequestHeader('Accept', 'application/xml');
        req.setRequestHeader('Authorization', 'Bearer ' + keycloak.token);
        req.send();
        if(req.status === 200){
              return $(req.responseText).find('ns2\\:cte_id').text();
        }else{
            return "notfound";
        }
    }


    //------------REQUEST BODYs-------

    function makeCapabilityTypeBody(formContent){
        //form variables eg. <input type="text" name="name" value="">
        var form_name = formContent.name.value;
        var form_reqmsgtype = formContent.reqmsgtype.value;
        var form_resmsgtype = formContent.resmsgtype.value;
        var form_schmlocation = formContent.schmlocation.value;
        var form_ctxpath = formContent.ctxpath.value;
        //Body XML
        var body='<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' +
            '<capabilityType xmlns="http://www.limetri.eu/schemas/ygg">' +
            '<name>' + form_name + '</name>' +
            '<requestMessageType>' + form_reqmsgtype + '</requestMessageType>' +
            '<responseMessageType>' + form_resmsgtype + '</responseMessageType>' +
            '<schemaLocation>' + form_schmlocation + '</schemaLocation>' +
            '<contextPath>' + form_ctxpath + '</contextPath>' +
            '</capabilityType>';
        return body;
    }

    function makeCapabilityBody(formContent){
        var form_desc = formContent.description.value;
        var form_uri = formContent.uri.value;
        var form_cte = formContent.cte_id.value;
        //Body XML
        var body='<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' + 
        '<Capability xmlns="http://www.limetri.eu/schemas/ygg">' +
        '<description>' + form_desc + '</description>' +
        '<uri>' + form_uri + '</uri>' +
        '<cte_id>' + form_cte + '</cte_id>' +
        '</Capability>'; 
        return body;
    }

    function makeBusinessProcessTemplateBody(formContent){
        var form_name = formContent.name.value;
        var cap_type_ids = formContent.cap_type_ids.value.split(" ");
        //Body XML
        var body='<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' +
            '<BusinessProcessTemplate xmlns="http://www.limetri.eu/schemas/ygg">' +
            '<name>' + form_name + '</name>';
        for (var i = 0; i < cap_type_ids.length; i++) {
            body = body + '<capabilityTypes>' +
            '<id>' + cap_type_ids[i] + '</id>' +
            '<name>name</name>' +
            '<requestMessageType>request</requestMessageType>' +
            '<responseMessageType>response</responseMessageType>' +
            '<schemaLocation>schema</schemaLocation>' +
            '<contextPath>eu.fispace.api.lg</contextPath>' +
            '</capabilityTypes>';
        }
        body = body + '</BusinessProcessTemplate>';
        return body;
    }

    function makeBusinessProcessTemplateBodyWithRoles(formContent, roles){
        var form_name = formContent.name.value;
        //Body XML
        var body='<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' +
            '<BusinessProcessTemplate xmlns="http://www.limetri.eu/schemas/ygg">' +
            '<name>' + form_name + '</name>';
        //Roles
        for (var rol = 0; rol < roles.length; rol++){
            body = body + '<role><id>'+ roles[rol].name + '</id>';
            //Capabilities Types Ids
            for (var i = 0; i < roles[rol].cap_type_ids.length; i++) {
                body = body + '<capabilityType>' +
                '<id>' + roles[rol].cap_type_ids[i] + '</id>' +
                '<name>name</name>' +
                '<requestMessageType>request</requestMessageType>' +
                '<responseMessageType>response</responseMessageType>' +
                '<schemaLocation>schema</schemaLocation>' +
                '<contextPath>eu.fispace.api.lg</contextPath>' +
                '</capabilityType>';
            }
            body = body + '</role>';
        }
        body = body + '</BusinessProcessTemplate>';
        return body;
    }

    function makeBusinessProcessBody(formContent, url){
        var form_name = formContent.name.value;
        var form_bpt_id = formContent.bpt_id.value;
        var cap_ids = formContent.cap_ids.value.split(" ");
        //Body XML
        var body='<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' +
            '<BusinessProcess xmlns="http://www.limetri.eu/schemas/ygg">' +
            '<name>' + form_name + '</name>' +
            '<bpt_id>' + form_bpt_id + '</bpt_id>';
        for (var i = 0; i < cap_ids.length; i++) {
            body = body + '<capabilities>' +
            '<id>' + cap_ids[i] + '</id>' +
            '<description>description</description>' +
            '<uri>uri</uri>' +
            '<cte_id>'+ getCapType(url.concat('/capabilities/'+cap_ids[i]))+'</cte_id>' +
            '</capabilities>';
        }
        body = body + '</BusinessProcess>';
        console.log(body);
        return body;
    }

    function makeBusinessProcessBodyWithRoles(formContent, roles, url){
        console.log(roles);
        var form_name = formContent.name.value;
        var form_bpt_id = formContent.bpt_id.value;
        //Body XML
        var body='<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' +
            '<BusinessProcess xmlns="http://www.limetri.eu/schemas/ygg">' +
            '<name>' + form_name + '</name>' +
            '<bpt_id>' + form_bpt_id + '</bpt_id>';
        //Roles
        for (var rol = 0; rol < roles.length; rol++){
            body = body + '<role><id>'+ roles[rol].name + '</id>';
            for (var i = 0; i < roles[rol].cap_ids.length; i++) {
                body = body + '<capability>' +
                '<id>' + roles[rol].cap_ids[i] + '</id>' +
                '<description>description</description>' +
                '<uri>uri</uri>' +
                '<cte_id>'+ getCapType(url.concat('/capabilities/'+roles[rol].cap_ids[i]))+'</cte_id>' +
                '</capability>';
            }
            body = body + '</role>';
        }
        body = body + '</BusinessProcess>';
        console.log(body);
        return body;
    }

    function throwLoginError(){
        alert("Auth Error. Please log in");
    }

    //----------PUBLIC VARIABLE----------
    function define_jsConnector(){
        var JsConnector = {};
        var name = "Javascript FiSpace connector";
        
        //----------------API URL---------------------
        var SDIURLDefault = "https://sdi-pie.fispace.eu:8443/sdi/admin-api";
        var SDIURLDefault_Use = "https://sdi-pie.fispace.eu:8443/sdi/api";
        //----------------/API URL---------------------

        //Login into Keycloak
        JsConnector.login = function(){
            keycloak.login();
        };
        //-----------------------------------API--------------------------------------    
        //--------------GETs-----------
        //GET CapabilityType
        JsConnector.getCapabilityTypes = function(value, fn){
            console.log("Get Capability types");
            keycloak.updateToken(10).success(function(){
                //Final URL
                var finalUrl = SDIURLDefault.concat('/capability-types');
                if(value !== ""){
                    alert("value: "+value);
                    finalUrl = finalUrl.concat('/'+value);
                }
                getAsText(finalUrl,fn);
            }).error(function() {
                throwLoginError();
            });
        };
        //GET Capabilities
        JsConnector.getCapabilities = function(value, fn){
            console.log("Get Capabilities");
            keycloak.updateToken(10).success(function(){
                 //Final URL
                var finalUrl = SDIURLDefault.concat('/capabilities');
                if(value !== ""){
                    finalUrl = finalUrl.concat('/'+value);
                }
                getAsText(finalUrl,fn);
            }).error(function() {
                throwLoginError();
            });
        };
        //GET business-process-templates
        JsConnector.getBusinessProcessTemplates = function(value, fn){
            console.log("Get Business Process Templates");
            keycloak.updateToken(10).success(function(){
                 //Final URL
                var finalUrl = SDIURLDefault.concat('/business-process-templates');
                if(value !== ""){
                    finalUrl = finalUrl.concat('/'+value);
                }
                getAsText(finalUrl,fn);
            }).error(function() {
                throwLoginError();
            });
        };
        //GET business-processes
        JsConnector.getBusinessProcesses = function(value, fn){
            console.log("Get Business processes");
            keycloak.updateToken(10).success(function(){
                 //Final URL
                var finalUrl = SDIURLDefault.concat('/business-processes');
                if(value !== ""){
                    finalUrl = finalUrl.concat('/'+value);
                }
                getAsText(finalUrl,fn);
            }).error(function() {
                throwLoginError();
            });
        };
        //-------------PUTs-----------
        //PUT CapabilityType
        JsConnector.putNewCapabilityType = function(formContent,fn){
            console.log("Test Post new Capability Type");
            keycloak.updateToken(10).success(function(){
                 //Final URL
                var finalUrl = SDIURLDefault.concat('/capability-types');
                //Body
                var requestBody = makeCapabilityTypeBody(formContent);
                putContent(requestBody,finalUrl,fn);
            }).error(function() {
                throwLoginError();
            });
        };
        //PUT Capability
        JsConnector.putNewCapability = function(formContent,fn){
            console.log("Test Post new Capability");
            keycloak.updateToken(10).success(function(){
                 //Final URL
                var finalUrl = SDIURLDefault.concat('/capabilities');
                //Body
                var requestBody = makeCapabilityBody(formContent);
                putContent(requestBody,finalUrl,fn);
            }).error(function() {
                throwLoginError();
            });
        };
        //PUT BusinessProcessTemplates
        JsConnector.putNewBusinessProcessTemplate = function(formContent,fn){
            console.log("Test Post new business process template");
            keycloak.updateToken(10).success(function(){
                 //Final URL
                var finalUrl = SDIURLDefault.concat('/business-process-templates');
                //Body
                var requestBody = makeBusinessProcessTemplateBody(formContent);
                putContent(requestBody,finalUrl,fn);
            }).error(function() {
                throwLoginError();
            });
        };
        //PUT BusinessProcessTemplates With Roles
        JsConnector.putNewBusinessProcessTemplateWithRoles = function(formContent,roles,fn){
            console.log("Test Post new business process template");
            keycloak.updateToken(10).success(function(){
                 //Final URL
                var finalUrl = SDIURLDefault.concat('/business-process-templates');
                //Body
                var requestBody = makeBusinessProcessTemplateBodyWithRoles(formContent,roles);
                putContent(requestBody,finalUrl,fn);
            }).error(function() {
                throwLoginError();
            });
        };
        //PUT BusinessProcess
        JsConnector.putNewBusinessProcess = function(formContent,fn){
            console.log("Test Post new business process");
            keycloak.updateToken(10).success(function(){
                 //Final URL
                var finalUrl = SDIURLDefault.concat('/business-processes');
                //Body
                var requestBody = makeBusinessProcessBody(formContent, SDIURLDefault);
                putContent(requestBody,finalUrl,fn);
            }).error(function() {
                throwLoginError();
            });
        };
        //PUT BusinessProcess With Roles
        JsConnector.putNewBusinessProcessWithRoles = function(formContent,roles,fn){
            console.log("Test Post new business process");
            keycloak.updateToken(10).success(function(){
                 //Final URL
                var finalUrl = SDIURLDefault.concat('/business-processes');
                //Body
                var requestBody = makeBusinessProcessBodyWithRoles(formContent, roles, SDIURLDefault);
                putContent(requestBody,finalUrl,fn);
            }).error(function() {
                throwLoginError();
            });
        };
        //------------DELETEs--------------
        //DELETE CapabilityType
        JsConnector.deleteCapabilityType = function(value,fn){
            keycloak.updateToken(10).success(function(){
                 //Final URL
                var finalUrl = SDIURLDefault.concat('/capability-types');
                //get the id
                if(value !== null || value !== ""){
                    finalUrl = finalUrl.concat('/'+value);
                }else{alert("No id found");}
                sendDelete(finalUrl,fn);
            }).error(function() {
                throwLoginError();
            });
        };
        //DELETE Capability
        JsConnector.deleteCapability = function(value,fn){
            keycloak.updateToken(10).success(function(){
                 //Final URL
                var finalUrl = SDIURLDefault.concat('/capabilities');
                //get the id
                if(value !== null || value !== ""){
                    finalUrl = finalUrl.concat('/'+value);
                }else{alert("No id found");}
                sendDelete(finalUrl,fn);
            }).error(function() {
                throwLoginError();
            });
        };
        //DELETE Business Process Template
        JsConnector.deleteBusinessProcessTemplate = function(value,fn){
            keycloak.updateToken(10).success(function(){
                 //Final URL
                var finalUrl = SDIURLDefault.concat('/business-process-templates');
                //get the id
                if(value !== null || value !== ""){
                    finalUrl = finalUrl.concat('/'+value);
                }else{alert("No id found");}
                sendDelete(finalUrl,fn);
            }).error(function() {
                throwLoginError();
            });
        };
        //DELETE Business Process
        JsConnector.deleteBusinessProcess = function(value,fn){
            keycloak.updateToken(10).success(function(){
                 //Final URL
                var finalUrl = SDIURLDefault.concat('/business-processes');
                //get the id
                if(value !== null || value !== ""){
                    finalUrl = finalUrl.concat('/'+value);
                }else{alert("No id found");}
                sendDelete(finalUrl,fn);
            }).error(function() {
                throwLoginError();
            });
        };
        //------------USE--------------
        //USE Capability
        JsConnector.useCapability = function(captype_name,bodyToSent,fn){
            keycloak.updateToken(10).success(function(){
                 //Final URL
                var finalUrl = SDIURLDefault_Use.concat('/capabilities/'+captype_name+'/use');
                postContent(bodyToSent,finalUrl,fn);
            }).error(function() {
                throwLoginError();
            });
        };
        //-----------------------------------/API------------------------------------------

        //return the main object
        return JsConnector;
    }
    //define globally if it doesn't already exist
    if(typeof(JsConnector) === 'undefined'){
        window.JsConnector = define_jsConnector();
    }
    else{
        console.log("Library already defined.");
    }
})(window);